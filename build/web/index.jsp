<%-- 
    Document   : index
    Created on : Apr 30, 2018, 10:51:12 AM
    Author     : Nguyen Thanh Nam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>shop</title>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <form method="POST">
            <input type="text" id="key" placeholder="tim kiem"/>
        </form>
        <p><button onclick="loadXMLDoc()">Tìm kiếm</button></p>
        <table id="demo" border="1">
        </table>

        <script>
            function loadXMLDoc() {
                var key = document.getElementById("key").value;
                var xmlhttp = new XMLHttpRequest();
                if(key=="")
                    var url = "http://localhost:8080/BookStoreServer/webresources/bookstoreWS/danhsach";
                else
                    var url = "http://localhost:8080/BookStoreServer/webresources/bookstoreWS/timkiem/"+key;
                xmlhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        myFunction(this);
                    }
                };
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            }

            function myFunction(xml) {
                var x, i, xmlDoc, table;
                xmlDoc = xml.responseXML;
                table = "<tr><th>Mã</th><th>Tên Sách</th><th>Ảnh</th><th>Thể Loại</th><th>NXB</th><th>Năm XB</th><th>Tác giả</th><th>Tái Bản</th><th>Giá bán</th></tr>";
                x = xmlDoc.getElementsByTagName("Sach")
                for (i = 0; i < x.length; i++) {
                    table += "<tr><td>" +
                            x[i].getElementsByTagName("ma")[0].childNodes[0].nodeValue +
                            "</td><td>" 
                            +"<a href=\"chitiet.jsp?ma="+x[i].getElementsByTagName("ma")[0].childNodes[0].nodeValue+"\">"+x[i].getElementsByTagName("tensach")[0].childNodes[0].nodeValue +
                            "</a></td><td>"
                            +"<img src= \""+x[i].getElementsByTagName("anh")[0].childNodes[0].nodeValue +
                            "\"/></td><td>"
                            +x[i].getElementsByTagName("theloai")[0].childNodes[0].nodeValue +
                            "</td><td>"
                            +x[i].getElementsByTagName("NXB")[0].childNodes[0].nodeValue +
                            "</td><td>"
                            +x[i].getElementsByTagName("namXB")[0].childNodes[0].nodeValue +
                            "</td><td>"
                            +x[i].getElementsByTagName("tacgia")[0].childNodes[0].nodeValue +
                            "</td><td>"
                            +x[i].getElementsByTagName("taiban")[0].childNodes[0].nodeValue +
                            "</td><td>"
                            +x[i].getElementsByTagName("giaban")[0].childNodes[0].nodeValue +
                            "</td></tr>";
                }
                document.getElementById("demo").innerHTML = table;
            }
        </script>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>
