<%-- 
    Document   : chitiet
    Created on : Apr 30, 2018, 5:18:17 PM
    Author     : Nguyen Thanh Nam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chi Tiet</title>
    </head>
    <body>
        <b id="demo">
        </b>
        <form method="POST">
            <input type="text" placeholder="số lượng">
            <input type="submit" value="thêm vào giỏ hàng">
        </form>

        <script>
            function GetURLParameter(sParam) {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return sParameterName[1];
                    }
                }
            }
            var ma = GetURLParameter("ma");
            var xmlhttp = new XMLHttpRequest();
            var url = "http://localhost:8080/BookStoreServer/webresources/bookstoreWS/timkiem/ma/" + ma;
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    myFunction(this);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();


            function myFunction(xml) {
                var x, xmlDoc, chitiet;
                xmlDoc = xml.responseXML;
                x = xmlDoc.getElementsByTagName("Sach")
                chitiet = "<img src= \"" + x[0].getElementsByTagName("anh")[0].childNodes[0].nodeValue +
                        "\"/>" +
                        "<br> tên sách : "
                        + x[0].getElementsByTagName("tensach")[0].childNodes[0].nodeValue +
                        "<br> thể loại : "
                        + x[0].getElementsByTagName("theloai")[0].childNodes[0].nodeValue +
                        "<br> Tác giả : "
                        + x[0].getElementsByTagName("tacgia")[0].childNodes[0].nodeValue +
                        "<br> Giá bán : "
                        + x[0].getElementsByTagName("giaban")[0].childNodes[0].nodeValue +
                        "<br> Mô tả : "
                        + x[0].getElementsByTagName("mota")[0].childNodes[0].nodeValue;
                document.getElementById("demo").innerHTML = chitiet;
            }
        </script>
    </body>
</html>
