package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>shop</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("        <form method=\"POST\">\n");
      out.write("            <input type=\"text\" id=\"key\" placeholder=\"tim kiem\"/>\n");
      out.write("        </form>\n");
      out.write("        <p><button onclick=\"loadXMLDoc()\">Tìm kiếm</button></p>\n");
      out.write("        <table id=\"demo\" border=\"1\">\n");
      out.write("        </table>\n");
      out.write("\n");
      out.write("        <script>\n");
      out.write("            function loadXMLDoc() {\n");
      out.write("                var key = document.getElementById(\"key\").value;\n");
      out.write("                var xmlhttp = new XMLHttpRequest();\n");
      out.write("                if(key==\"\")\n");
      out.write("                    var url = \"http://localhost:8080/BookStoreServer/webresources/bookstoreWS/danhsach\";\n");
      out.write("                else\n");
      out.write("                    var url = \"http://localhost:8080/BookStoreServer/webresources/bookstoreWS/timkiem/\"+key;\n");
      out.write("                xmlhttp.onreadystatechange = function () {\n");
      out.write("                    if (this.readyState == 4 && this.status == 200) {\n");
      out.write("                        myFunction(this);\n");
      out.write("                    }\n");
      out.write("                };\n");
      out.write("                xmlhttp.open(\"GET\", url, true);\n");
      out.write("                xmlhttp.send();\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            function myFunction(xml) {\n");
      out.write("                var x, i, xmlDoc, table;\n");
      out.write("                xmlDoc = xml.responseXML;\n");
      out.write("                table = \"<tr><th>Mã</th><th>Tên Sách</th><th>Ảnh</th><th>Thể Loại</th><th>NXB</th><th>Năm XB</th><th>Tác giả</th><th>Tái Bản</th><th>Giá bán</th></tr>\";\n");
      out.write("                x = xmlDoc.getElementsByTagName(\"Sach\")\n");
      out.write("                for (i = 0; i < x.length; i++) {\n");
      out.write("                    table += \"<tr><td>\" +\n");
      out.write("                            x[i].getElementsByTagName(\"ma\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\" \n");
      out.write("                            +\"<a href=\\\"chitiet.jsp?ma=\"+x[i].getElementsByTagName(\"ma\")[0].childNodes[0].nodeValue+\"\\\">\"+x[i].getElementsByTagName(\"tensach\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</a></td><td>\"\n");
      out.write("                            +\"<img src= \\\"\"+x[i].getElementsByTagName(\"anh\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"\\\"/></td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"theloai\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"NXB\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"namXB\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"tacgia\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"taiban\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td><td>\"\n");
      out.write("                            +x[i].getElementsByTagName(\"giaban\")[0].childNodes[0].nodeValue +\n");
      out.write("                            \"</td></tr>\";\n");
      out.write("                }\n");
      out.write("                document.getElementById(\"demo\").innerHTML = table;\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footer.jsp", out, false);
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
